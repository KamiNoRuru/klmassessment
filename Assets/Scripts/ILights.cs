using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILights
{
    public void EnableLight();
    public void DisableLight();
}
