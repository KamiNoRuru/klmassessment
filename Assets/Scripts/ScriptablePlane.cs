using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "Airplane", menuName = "ScriptableObjects/AirPlane")]
public class ScriptablePlane : ScriptableObject
{
    [SerializeField] private string type;
    [SerializeField] private string merk;

    public string PType { get => type; }
    public string PMerk { get => merk; }
}
