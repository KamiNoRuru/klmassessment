using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPark
{
    public void GetParkingSpot(Vector3 parkingSpotLocation);
    public void Park();
}
