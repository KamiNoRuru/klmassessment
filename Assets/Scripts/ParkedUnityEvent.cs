using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ParkedUnityEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent _parkedPlane;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("AirPlane"))
        {
            Debug.Log("Has entered the hangar");
            _parkedPlane.Invoke();
        }
    }
}
