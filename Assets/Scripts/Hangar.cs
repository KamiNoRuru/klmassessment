using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Hangar : MonoBehaviour
{
    public int hangarID;
    public string HangarID { get { return hangarID.ToString(); } }
    [SerializeField] private TMP_Text hangarText;

    private Light _parkedFeedback;

    private void Start()
    {
        _parkedFeedback = transform.GetComponentInChildren<Light>();
        _parkedFeedback.color = Color.red;

        hangarText = transform.GetComponentInChildren<TMP_Text>();
        hangarText.text = hangarID.ToString() ;
    }

    public void ChangeLightColour()
    {
        StartCoroutine(Parking());
    }

    private IEnumerator Parking()
    {
        _parkedFeedback.color = Color.blue;
        yield return new WaitForSeconds(2f);
        _parkedFeedback.color = Color.green;
    }
}
