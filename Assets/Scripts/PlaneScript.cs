using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlaneScript : MonoBehaviour, IPark, ILights
{
    public ScriptablePlane planeInformation;
    public Vector3 parkingSpot;
    [SerializeField]private bool isParking = false;

    [SerializeField] private Light headLight;
    private string _typePlane;
    private string _planeMerk;
    public int planeID;

    #region NavMeshMovement

    public float radius;
    private NavMeshAgent agent;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        _typePlane = planeInformation.PType.ToString();
        _planeMerk = planeInformation.PMerk.ToString();
        headLight = gameObject.GetComponentInChildren<Light>();

        transform.name = "Plane #" + planeID +  " " + _planeMerk + " " +_typePlane;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.hasPath && !isParking) agent.SetDestination(GetPoint.Instance.GetRandomPoint(transform, radius));
    }

    public void GetParkingSpot(Vector3 parkingSpotLocation)
    {
        parkingSpot = parkingSpotLocation;
    }

    public void Park()
    {
        isParking = true;
        agent.SetDestination(parkingSpot);
    }

    public void EnableLight()
    {
        if (!headLight.enabled) headLight.enabled = true;
        else DisableLight();
    }

    public void DisableLight()
    {
        headLight.enabled = false;
    }

    /*
#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }

#endif*/
}
