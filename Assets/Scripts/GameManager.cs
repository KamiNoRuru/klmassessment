using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<GameObject> hangarsList = new List<GameObject>();
    public List<GameObject> planeList = new List<GameObject>();

    public int ID;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (var hangar in hangarsList)
        {
            ID++;
            #region Hangar section
            var hangarScript = hangar.GetComponent<Hangar>();
            hangarScript.hangarID = ID;
            hangar.transform.name = "Hangar #" + ID;
            #endregion

            #region Plane section
            var plane = planeList[ID - 1].GetComponent<PlaneScript>();
            plane.planeID = ID;
            plane.GetParkingSpot(hangarScript.GetComponentInChildren<Transform>().position);
            
            #endregion
        }
    }

    public void Park()
    {
        foreach (var plane in planeList)
        {
            plane.GetComponent<PlaneScript>().Park();
        }
    }

    public void TurnOnLights()
    {
        foreach (var plane in planeList)
        {
            plane.GetComponent<PlaneScript>().EnableLight();
        }
    }
}
